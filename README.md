# KMS

## Overview

KMS Module

## Usage

```hcl
module "KMS" {
  source = "git::ssh://"
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| null | ~> 2.1 |

## Providers

| Name | Version |
|------|---------|
| null | ~> 2.1 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| mandatory | this field is mandatory | `string` | n/a | yes |
| optional | this field is optional | `string` | `"default_value"` | no |

## Outputs

| Name | Description |
|------|-------------|
| output\_name | description for output\_name |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Development

### Prerequisites

- [terraform](https://learn.hashicorp.com/terraform/getting-started/install#installing-terraform)
- [terraform-docs](https://github.com/segmentio/terraform-docs)
- [pre-commit](https://pre-commit.com/#install)

### Configurations

- Configure pre-commit hooks
```sh
pre-commit install
```




### Tests

- Tests are available in `test` directory



## Authors

This project is authored by below people

- Soham Dutta