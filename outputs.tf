output "key" {
  value       = aws_kms_key.module.*
  description = "Key"
}

output "alias" {
  value       = aws_kms_alias.module.*
  description = "Alias"
}

output "grant" {
  value       = aws_kms_grant.module
  description = "Grant"
}