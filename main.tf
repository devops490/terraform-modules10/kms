resource "aws_kms_key" "module" {
  count                   = var.enabled ? 1 : 0
  is_enabled              = var.enabled
  deletion_window_in_days = var.deletion_window_in_days
  enable_key_rotation     = var.enable_key_rotation
  policy                  = var.policy
  tags                    = var.tags
  description             = var.description
}

resource "aws_kms_alias" "module" {
  count         = var.enabled ? 1 : 0
  name          = var.alias
  target_key_id = aws_kms_key.module[count.index].id
}

resource "aws_kms_grant" "module" {
  count         = var.enabled ? 1 : 0
  name              = "${var.alias}-grant"
  key_id            = aws_kms_key.module.key_id
  grantee_principal = aws_iam_role.module.arn
  operations        = var.grant_operations
  constraints {
    encryption_context_subset = var.encryption_context
  }
}

resource "aws_kms_external_key" "module" {
  count                   = var.external_key ? 1 : 0
  deletion_window_in_days = var.deletion_window_in_days
  description             = var.description
  enabled                 = var.enabled
  key_material_base64     = var.key_material_base64
  policy                  = var.policy
  tags                    = var.tags
}